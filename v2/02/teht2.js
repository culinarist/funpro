'use strict';

const Auto  = (function(){
    
    const suojatut = new WeakMap();  // yhteinen ilmentymille
    
    class Auto {
        constructor(){
            suojatut.set(
                    this,
                    {tankki: 0, matkamittari: 0}
                );
        }  // suojatut instanssimuuttujat tankki ja matkamittari
        
        getTankki() {return suojatut.get(this).tankki;}
        getMatkamittari() {return suojatut.get(this).matkamittari;}
        
        aja() {
            if(this.getTankki() > 0) {
                let auto = suojatut.get(this);
                auto.matkamittari += 1;
                auto.tankki -= 1;
            }
        }
        
        tankkaa(x) {
            if(x > 0) {
                let auto = suojatut.get(this);
                auto.tankki += x;
            }
        }
        
    }
    
    return Auto;
})();

const auto = new Auto();
auto.tankkaa(10);
console.log(auto.getMatkamittari());
console.log(auto.getTankki());
auto.aja();
console.log(auto.getMatkamittari());
console.log(auto.getTankki());