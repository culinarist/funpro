// Funktio jolle annetaan k piste ja lisäpisteet parametrein ja palauttaa funktion
// joka ottaa parametrikseen vain pituuden

function maki(kPiste, lisapiste) {
    return function pisteet(pituus) {
        return kPiste == pituus ? 60 : ((pituus - kPiste) * lisapiste) + 60;
    };
}

let normMaki = maki(75, 2);
let isoMaki = maki(100, 1.8);

let normPisteet = normMaki(78);
let normPisteetH = normMaki(50);
let isoPisteet = isoMaki(120);

console.log(normPisteet);
console.log(isoPisteet);
console.log(normPisteetH);
