const Immutable = require('immutable');

let number = Immutable.Repeat()
                      .map( (x = 1) => x + 1 )
                      .take( 10 )
                      .toJSON();

console.log(number);
