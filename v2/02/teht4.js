'use strict';

const Immutable = require('immutable');

let tulos = Immutable.Range(1, Infinity)
  .map(function(n){console.log(`map ${n}`); return n + 2})   // Tietenkään ei mapata ääretöntä :D 
  .filter(n => n % 13 === 0)                                // Filteröidään vain 13 tasan jaolliset
  .take(2)                                                  // Otetaan taulukon kaksi ensimmäistä
  .reduce((a, n) => a += n, 0);                             // Lisätään ensimäiseen toinen
  
console.log(tulos);
