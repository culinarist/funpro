const Immutable = require('immutable');

const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);

const set2 = set1.add('ruskea');
const set3 = set2.add('ruskea');

console.log(set1);
console.log(set2);
console.log(set3);

console.log("set1 and set2");
if (set1 === set2) console.log("TRUE"); else console.log("FALSE");

console.log("set2 and set3");
if (set2 === set3) console.log("TRUE"); else console.log("FALSE");

