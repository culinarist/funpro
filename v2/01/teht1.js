'use strict'

let toCelsius = fahrenheit => (5/9) * (fahrenheit-32);
let area = radius => Math.PI * radius * radius;

console.log(toCelsius(10));
console.log(area(5));