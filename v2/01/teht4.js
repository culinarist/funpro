let yearA = [20, 21, 23, 16, 13, 10, 8, 4, 1, 0, -2, -5];
let yearB = [19, 22, 21, 15, 16, 11, 4, 0, -1, -6, -4, -12];
let avg = [];

let mean = (a, b, i) => {
    return (a[i] + b[i]) / 2;
};

for (let i = 0; i < yearA.length; i++){
    avg.push(mean(yearA,yearB,i));
}

let removeNeg = item => {
    if(item >= 0) {
        return true;
    }
    return false;
};

let getAvg = (acc, curr, currIndx, array) => {
    if(currIndx == array.length - 1) {
        return (acc + curr)/ array.length;
    }
    return acc + curr;
};

let final = avg.filter(removeNeg)
            .reduce(getAvg);

console.log(final);

