let fs = require("fs");

let source = fs.readFileSync('kalevala.txt', 'utf8');

let replacedSource = source.replace(/[^a-zA-Z0-9 ]/g, '');
replacedSource = replacedSource.replace(/\s{2,}/g, ' ').toLowerCase();

console.log(replacedSource);

let words = replacedSource.split(' ');

let wordcnt = words.reduce(function(map, word) {
                            map[word] = (map[word]||0)+1;
                            return map;
                            }, Object.create(null)
                        );

console.log(wordcnt);
