abstract class Lotto {  
    abstract IntSupplier arvo();
}

public class Streams {
    
    public static void main(String[] args) {
        
        System.out.println("Anonyymi sisäluokka lotto: ");
        Lotto lotto = new Lotto(){
            @Override
            public IntSupplier arvo() {
                return () -> ThreadLocalRandom.current().nextInt(1, 40);
            }
            
        };
        IntStream.generate(lotto.arvo())
                .distinct()
    		.limit(7)
                .sorted()
                .forEach(System.out::println);
        
        System.out.println("\nLambda lotto: ");

        IntStream.generate(() -> ThreadLocalRandom.current().nextInt(1, 40))
                .distinct()
    		.limit(7)
                .sorted()
                .forEach(System.out::println);
        
        
    	
          
    }
    
}