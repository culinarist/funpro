import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class Streams {
          
    public static void main(String[] args) {
               
       Function siirto = Piste.makeSiirto(1, 2);
       Function skaalaus = Piste.makeSkaalaus(2);
       Function kierto = Piste.makeKierto(Math.PI);
       Function<Piste, Piste> muunnos = siirto.andThen(skaalaus).andThen(kierto);
       
       Piste[] pisteet = {new Piste(1,1), new Piste(2,2), new Piste(3,3)};
       List<Piste> uudetPisteet = new CopyOnWriteArrayList();
       
       for (Piste p: pisteet){
           uudetPisteet.add((Piste)muunnos.apply(p));
       } 
  
       uudetPisteet.forEach(p -> System.out.println("X: " + p.getX() + " Y: " + p.getY()));
    }
    
    
}

class Piste {
        
        double x, y;
        
        public Piste(double x, double y) {
            this.x = x;
            this.y = y;
        }
        
        public double getX() {
        	return x;
        }
        
        public double getY() {
        	return y;
        }
        
        public static <T, R> Function<Piste, Piste> makeSiirto(int i, int i0) {
            return (piste) -> new Piste(piste.getX() + i, piste.getX() + i0);
        }

        public static <T, R> Function<Piste, Piste> makeSkaalaus(int i) {
        	return (piste) -> new Piste(piste.getX() * i, piste.getY() * i);
        }

        public static <T, R> Function<Piste, Piste> makeKierto(double PI) {
        	return (piste) -> new Piste(piste.getX() * Math.cos(PI) - piste.getY() * Math.sin(PI),
        			piste.getX() * Math.sin(PI) + piste.getY() * Math.cos(PI));
        }
        
}