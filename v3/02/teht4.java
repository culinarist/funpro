public class Teht4 {

    public static void main(String[] args) {
        IntSupplier fibo = new IntSupplier() {
            private int edellinen = 0;
            private int nykyinen = 0;

            @Override
            public int getAsInt() {
                if (nykyinen == 0) {
                    int nextValue = 1;
                    this.edellinen = this.nykyinen;
                    this.nykyinen = nextValue;
                    return 0;
                } else {
                    int nextValue = this.edellinen + this.nykyinen;
                    this.edellinen = this.nykyinen;
                    this.nykyinen = nextValue;
                    return this.edellinen;
                }
                
            }
        };

        IntStream.generate(fibo).limit(10).forEach(System.out::println);
    }

}