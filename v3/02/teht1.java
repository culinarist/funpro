Supplier<Integer> generaattori1 = () -> 2;
Supplier<Integer> generaattori2 = () -> (int)(Math.random() * 6 + 1);
        
System.out.println(generaattori1.get());
System.out.println(generaattori2.get());