import java.util.function.Function;

public class Main {
	

	public static void main(String[] args) {
		
		Function<Double,Double> toCelsius = fahrenheit -> (double) ( (fahrenheit - 32) * 5/9);
        double celsius = toCelsius.apply(100.0);
        System.out.println(celsius);
        
        Function<Double,Double> area = radius -> (double) (Math.PI * radius * radius);
        double radius = area.apply(5.0);
        System.out.println(radius);

	}

}