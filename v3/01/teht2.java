List<Transaction> tr2012 = transactions.stream()
                .filter(transaction -> transaction.getYear() >= 2012)
                .filter(transaction -> transaction.getValue() >= 900)
                .sorted(comparing(t -> t.getValue()))
                .collect(toList());
        System.out.println(tr2012);
        
// Palauttaa
// [{Trader:Alan in Cambridge, year: 2012, value:950}, {Trader:Raoul in Cambridge, year: 2012, value:1000}]

private static Map<Dish.Type, Integer> countDishClasses() {
    	Map<Dish.Type, Integer> collect = 
    			Dish.menu.stream()
    			.map(dish -> dish.getType())
    			.collect(groupingBy(Function.identity(), summingInt(e -> 1)));
    	return collect;
}

// Palauttaa
// dish class count: {OTHER=4, FISH=2, MEAT=3}