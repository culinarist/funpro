List<Integer> list1 = Arrays.asList(1, 2, 3);
List<Integer> list2 = Arrays.asList(3, 4);

List<int[]> pairs = list1.stream().flatMap(
                            i -> list2.stream().map(
                                j -> new int[] { i, j }))
		 .collect(Collectors.toList());
		 
pairs.forEach(i -> {
    System.out.println("{" + i[0]+ "," + i[1]+ "}");
});