int randomNums = IntStream.generate(() -> ThreadLocalRandom.current().nextInt(1, 7))
    			.limit(20)
    			.filter(x -> x == 6)
    			.reduce(0, (x, y) -> x+y);
    	
System.out.println("Numbers: " + randomNums);

// tuottaa 20 random lukua 1-6 väliltä ja suodattaa
// niistä vain kutoset ja laskee ne yhteen