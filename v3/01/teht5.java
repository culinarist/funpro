import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;

public class teht5 {
    public static void main(String ...args){

        try {

            countWords(Paths.get("./src/kalevala.txt"));
            /*
            Files.lines(Paths.get("./src/kalevala.txt"), Charset.defaultCharset())
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .collect(Collectors.groupingBy(Function.identity(), TreeMap::new, counting())).entrySet()
                    .forEach(System.out::println);
            */


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void countWords(final Path file) throws IOException {
        Arrays.stream(new String(Files.readAllBytes(file), StandardCharsets.UTF_8).split("\\s+"))
                .collect(Collectors.groupingBy(Function.identity(), TreeMap::new, counting())).entrySet()
                .forEach(System.out::println);
    }
}