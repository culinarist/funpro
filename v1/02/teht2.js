'use strict'

const f = function () {
    return function(x, y){
        if(x > y){
          return 1;
      } else if (x < y) {
          return -1;
      }
      return 0;
    }
}();

let x = function (a, b, c) {
    let sum = 0;
    for(let i = 0; i < b.length; i++){
        if(a(b[i], c[i]) == 1){
            sum++;
        }
    }
    return sum;
}

let yearA = [20, 21, 23, 16, 13];
let yearB = [19, 22, 21, 15, 16];

console.log( x(f, yearA, yearB) );