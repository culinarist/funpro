'use strict'

const f = function () {
    return function(x, y){
        if(x > y){
          return 1;
      } else if (x < y) {
          return -1;
      }
      return 0;
    }
}();

console.log(f(1, 1));
console.log(f(1, 0));
console.log(f(0, 1));