function potenssi(x, y) {
    return potenssiHelper(x, y, 1);
}

function potenssiHelper(x, y, pow) {
    if(y == 0) return pow;
    return potenssiHelper(x, y-1, x * pow);
}

console.log(potenssi(2, 4));