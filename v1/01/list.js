function reverse(list) {
    if (!list.length) {
        return list;
    }
    return reverse(list.slice(1)).concat(list[0]);
}

var list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
console.log(reverse(list));