function onPalindromi(merkkijono) {
  if(merkkijono.length < 2){
      return true;
  } else if(merkkijono.charAt(0) != merkkijono.charAt(merkkijono.length-1)){
      return false;
  }
  merkkijono = merkkijono.substr(1);
  merkkijono = merkkijono.slice(0, -1);
  return onPalindromi(merkkijono);
}

console.log(onPalindromi('imaami'))