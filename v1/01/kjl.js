function kjl(p, q) {
    if (q == 0){
        if(p == 1){
            return true
        }
        return false;
    } 
    return kjl(q, p%q);
}

// False koska 14 / 7 = 2
console.log(kjl(14, 7));

// True suurin yhteinen tekijä on 1
console.log(kjl(35, 18));