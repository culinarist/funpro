(ns test-lein.core
  (:gen-class))

(defn -main [& args])

(defn square [num] (* num num) )
  
(defn karkausvuosi? [vuosi]
  (cond (zero? (mod vuosi 400)) true
        (zero? (mod vuosi 100)) false
        (zero? (mod vuosi 4)) true
        :default false))

;v6 01 teht 1
(def yearA [1, 2, 3, 4, 5, 10, 20, 13, -5, -10, -12, -1])
(def yearB [3, 1, 7, 4, 9, 12, 27, 33, -7, -18, -22, -10])
(def avg #(/ (+ %1 %2) 2))
(def sum #(reduce + %))
(def average #(/ (sum %) (count %)))
(println (filter #(>= (min %) 0) (map avg yearA yearB)))

(println 
  ( average (filter #(>= (min %) 0) (map avg yearA yearB)))
)

;; v6 01 teht 2
(def sumNeste #(reduce + :neste))

(defn nestem [])

(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])
 
(println 
  (sum
    (map #(- (get % :neste) (get % :vesi))
             (filter #(= (:kk %) 4 ) food-journal)))
)

;;v6 01 teht 3
(def kk (map  #(dissoc % :paiva :neste :vesi)
              (filter #(= (:kk %) 4) food-journal)))
                  
(def paiva (map  #(dissoc % :kk :neste :vesi)
                    (filter #(= (:kk %) 4) food-journal)))
                  
(def muuneste (map  #(- (get % :neste) (get % :vesi))
                    (filter #(= (:kk %) 4) food-journal)))

(defn unify-data [kk paiva muuneste]
  { :kk (get kk :kk)
    :paiva (get paiva :paiva)
    :muuneste muuneste
  }
)

(println (map unify-data kk paiva muuneste))