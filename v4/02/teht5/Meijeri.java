import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class Meijeri {

    Map<String, Supplier<Tuote>> map;

    public Meijeri(){
        map = new HashMap<>();
    }

    public Tuote getTuote(String tuoteTyyppi){
        Supplier<Tuote> tuote = map.get(tuoteTyyppi.toUpperCase());
        if(tuote != null) {
            return tuote.get();
        }
        throw new IllegalArgumentException("No such shape " + tuoteTyyppi.toUpperCase());
    }
}
