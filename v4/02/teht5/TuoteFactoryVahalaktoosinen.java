public class TuoteFactoryVahalaktoosinen extends Meijeri{

    public TuoteFactoryVahalaktoosinen() {
        map.put("MAITO", Maito::new);
        map.put("JUUSTO", Juusto::new);
        map.put("JUGURTTI", Jugurtti::new);
    }

}
