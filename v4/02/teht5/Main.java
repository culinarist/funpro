public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Meijeri factory = new TuoteFactoryVahalaktoosinen();

        Maito maito = (Maito) factory.getTuote("maito");
        Juusto juusto = (Juusto) factory.getTuote("juusto");
        Jugurtti jugurtti = (Jugurtti) factory.getTuote("jugurtti");

        maito.tulosta();
        juusto.tulosta();
        jugurtti.tulosta();
    }

}
