public class teht4 {

    public static void main(String[] args) {

        Stream<Omena> omenaStream = Stream.of(
                new Omena("vihreä", 200),
                new Omena("punainen", 200),
                new Omena("punainen", 100)
        );

        List<Omena> omenaLista = omenaStream.collect(
                ArrayList::new,
                List::add,
                List::addAll);

        System.out.println(omenaLista);

        // Annetaan supplier (Arraylist), accumulator ja combiner
        // Supplier tuottaa tyhjän kokoelman (Arraylistin)
        // Accumulator kerää jokaisen omenan yksitellen listaan
        // Combiner yhdistää kaikki accumulatorin keräämät omenat supplierin kokoelmaan eli Arraylistiin


    }
}
