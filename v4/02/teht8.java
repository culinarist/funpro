public class teht8 {

    public static void main(String[] args) {

        UutistoimistoSubject helsinginSanomat = new UutistoimistoSubject();
        UutistoimistoSubject iltaLehti = new UutistoimistoSubject();

        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();

        helsinginSanomat.addObserver((Observable o, Object arg) -> {
            String uutinen = arg.toString();
            String etsi  = "politiikka";
            if (uutinen.toLowerCase().contains(etsi.toLowerCase())) {
                list1.add(uutinen);
            }
        });

        iltaLehti.addObserver((Observable o, Object arg) -> {
            String uutinen = arg.toString();
            String etsi  = "julkkis";
            if (uutinen.toLowerCase().contains(etsi.toLowerCase())) {
                list2.add(uutinen);
            }
        });

        Thread t1 = new Thread(helsinginSanomat);
        t1.start();
        Thread t2 = new Thread(iltaLehti);
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException ex) {
        }

        list1.forEach(System.out::println);
        list2.forEach(System.out::println);

    }

}



public class UutistoimistoSubject extends Observable implements Runnable{

    String[] stories = new String[]{
            "politiikka 1",
            "politiikka 2",
            "politiikka jtn 3",
            "asdasdads",
            "joku turha julkkis",
            "asdasdads",
            "joku turha julkkis 2",
            "asd"
    };

    @Override
    public void run() {
        for (String story : stories) {
            try {
                setChanged();
                notifyObservers(story);
                sleep(100);
            } catch (InterruptedException ex) {
            }
        }
    }

}
