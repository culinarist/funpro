((fn [raja]
    (loop [iteration 1]
          (if (<= iteration raja)
              (if (= (mod iteration 3) 0)
                  (do (println iteration)
                      (recur (inc iteration)))
                  (recur (inc iteration)))))) 12)