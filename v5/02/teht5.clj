(defn syt [p q]
     (if (= q 0)
         p
         (recur q (mod p q))))
(syt 102 68)