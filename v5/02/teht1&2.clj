(loop [iteration 0]
      (if (= iteration 0)
          (println "Anna numero joka on suurempi kuin 0")
          (println "Numerosi oli pienempi tai yhtäsuuri kuin 0. Anna suurempi kuin 0"))
      (let [num (read-line)]
           (if (<= (Integer. num) 0)
               (recur (inc iteration))
               (if (= (mod (Integer. num) 2) 0)
                   (println "Numero on parillinen")
                   (println "Numero on pariton")))))